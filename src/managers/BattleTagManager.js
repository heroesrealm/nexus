/*
  REQUIREMENTS
*/
const { BattleTags } = require("../data/initModels");
const owHandler = require("../utils/classes/OverwatchHandler");

/*
  FUNCTIONS
*/
async function getBattleTag(btag) {
  return BattleTags.findOrCreate({ where: { battletag: btag } });
}

async function updateSR(btag) {
  const [battletag] = await getBattleTag(btag);
  if (battletag) {
    const search = await owHandler.search(btag);
    await owHandler.getOverall("pc", "eu", search[0].urlName).then(data => {
      if (Number.isNaN(data.profile.rank))
        battletag.update({ skillRating: -1 });
      else battletag.update({ skillRating: data.profile.rank });
      battletag.save();
    });
  }
}

/*
  EXPORTS
*/
module.exports = { getBattleTag, updateSR };
