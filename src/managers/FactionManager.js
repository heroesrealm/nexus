/*
  REQUIREMENTS
*/
const { Factions } = require("../data/initModels");

/*
  FUNCTIONS
*/
async function getFaction(name) {
  return Factions.findOne({ where: { name } });
}

/*
  EXPORTS
*/
module.exports = { getFaction };
