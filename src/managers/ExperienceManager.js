/*
  REQUIREMENTS
*/
const { Experiences } = require("../data/initModels");
const SeasonManager = require("./SeasonManager");

/*
  FUNCTIONS
*/
async function getExperience(user, season) {
  return Experiences.findOne({
    where: { userId: user.get("id"), seasonId: season.get("id") }
  });
}

async function getActualExperiences(user) {
  const actualSeason = await SeasonManager.getActualSeason();
  if (actualSeason) {
    const [season] = await SeasonManager.getSeason(actualSeason);
    return getExperience(user, season);
  }
  return undefined;
}

async function getTotalExperiences(user) {
  return Experiences.sum("count", { where: { userId: user.get("id") } });
}

async function getXPLimitByLevel(level) {
  return 5 * level ** 2 + 50 * level + 100;
}

async function getLevelByXP(_xp) {
  let xp = _xp;
  let level = 0;

  while (xp >= getXPLimitByLevel(level)) {
    const xpLimit = getXPLimitByLevel(level);
    xp -= xpLimit;
    level += 1;
  }

  return [xp, level];
}

async function createExperience(count, user, season) {
  const experience = await Experiences.create({ count });
  experience.setUser(user);
  experience.setSeason(season);
}

async function addExperience(experience, count) {
  const actualExperiences = experience.get("count");
  experience.update({ count: actualExperiences + count });
}

/*
  EXPORTS
*/
module.exports = {
  getExperience,
  getActualExperiences,
  getTotalExperiences,
  getLevelByXP,
  getXPLimitByLevel,
  createExperience,
  addExperience
};
