/*
  REQUIREMENTS
*/
const Sequelize = require("sequelize");

const { Op } = Sequelize;
const {
  database,
  host,
  user,
  password,
  dialect,
  logging
} = require("../settings.json");
const { Seasons } = require("../data/initModels");

/*
  PROPERTIES
*/
const sequelize = new Sequelize(database, user, password, {
  host,
  dialect,
  logging,
  dialectOptions: {
    useUTC: false // * for reading from database
  },
  timezone: "+02:00" // * for writing to database
});

/*
  FUNCTIONS
*/
async function getSeason(n) {
  return Seasons.findOrCreate({ where: { season: n } });
}

async function getActualSeason() {
  return Seasons.max("season", { where: { endedAt: null } });
}

async function getLastSeason() {
  return Seasons.max("season", { where: { endedAt: { [Op.ne]: null } } });
}

async function startSeason(n) {
  const [season] = await getSeason(n);
  if (season) {
    season.update({ startedAt: sequelize.fn("NOW") });
  }
}

async function endSeason(n) {
  const [season] = await getSeason(n);
  if (season) {
    season.update({ endedAt: sequelize.fn("NOW") });
  }
}

/*
  EXPORTS
*/
module.exports = {
  getSeason,
  getActualSeason,
  getLastSeason,
  startSeason,
  endSeason
};
