/*
  REQUIREMENTS
*/
const Sequelize = require("sequelize");
const {
  database,
  host,
  user,
  password,
  dialect,
  logging
} = require("../settings.json");
const { Users } = require("../data/initModels");
const FactionManager = require("./FactionManager");
const BattleTagManager = require("./BattleTagManager");
const SummonerManager = require("./SummonerManager");
const SeasonManager = require("./SeasonManager");
const ExperienceManager = require("./ExperienceManager");
const TokenManager = require("./TokenManager");
const logger = require("../utils/functions/getLogger");

/*
  PROPERTIES
*/
const sequelize = new Sequelize(database, user, password, {
  host,
  dialect,
  logging,
  dialectOptions: {
    useUTC: false // * for reading from database
  },
  timezone: "+02:00" // * for writing to database
});

/*
  FUNCTIONS
*/
async function getUser(id) {
  return Users.findOrCreate({ where: { discordID: id } });
}

async function setFaction(member, factionName, guild) {
  const faction = await FactionManager.getFaction(factionName);
  if (faction) {
    const [memberUser] = await getUser(member.id);
    if (memberUser) {
      memberUser.update({ dateJoinFaction: sequelize.fn("NOW") });
      faction.addUser(memberUser);

      const role = guild.roles.find(
        r =>
          r.name.toLowerCase().normalize("NFD") ===
          factionName.toLowerCase().normalize("NFD")
      );
      member.addRole(role).catch(err => logger.log("error", err));
    }
  }
}

async function removeFaction(member, guild) {
  const [memberUser] = await getUser(member.id);
  if (memberUser) {
    const faction = await memberUser.getFaction();
    if (faction) {
      faction.removeUser(memberUser);

      const factionName = faction.get("name");
      const role = guild.roles.find(r => r.name === factionName);
      member.removeRole(role).catch(err => logger.log("error", err));
    }
  }
}

async function addExperiences(member, count) {
  const [memberUser] = await getUser(member.id);
  if (memberUser) {
    const actualSeason = await SeasonManager.getActualSeason();
    const [season] = await SeasonManager.getSeason(actualSeason);
    if (actualSeason) {
      const experiences = await memberUser.getExperiences({
        where: { seasonId: season.get("id") }
      });
      if (experiences.length > 0)
        ExperienceManager.addExperience(experiences[0], count);
      else ExperienceManager.createExperience(count, memberUser, season);
    }
  }
}

async function addTokens(member, count) {
  const [memberUser] = await getUser(member.id);
  if (memberUser) {
    const tokens = await memberUser.getTokens();
    if (tokens.length > 0) TokenManager.addToken(tokens[0], count);
    else TokenManager.createToken(count, memberUser);
  }
}

async function updateData(member) {
  const [memberUser] = await getUser(member.id);
  if (memberUser) {
    memberUser.update({ discordTag: member.user.tag });

    const battletags = await memberUser.getBattletags();
    if (battletags) {
      await battletags.forEach(async battletag => {
        await BattleTagManager.updateSR(battletag.get("battletag"));
      });
    }

    const summoners = await memberUser.getSummoners();
    if (summoners) {
      await summoners.forEach(async summoner => {
        await SummonerManager.updateRank(
          summoner.get("accountId"),
          summoner.get("accountName")
        );
      });
    }
  }
}

/*
  EXPORTS
*/
module.exports = {
  getUser,
  setFaction,
  removeFaction,
  addExperiences,
  addTokens,
  updateData
};
