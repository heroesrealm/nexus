/*
  REQUIREMENTS
*/
const RiotHandler = require("leaguejs");
const { Summoners } = require("../data/initModels");
const { riotAPI } = require("../settings.json");
const getLogger = require("../utils/functions/getLogger");

const logger = getLogger();

/*
  FUNCTIONS
*/
async function getSummoner(accountName, accountId) {
  return Summoners.findOrCreate({ where: { accountName, accountId } });
}

async function updateRank(accountId, accountName) {
  const handler = new RiotHandler(riotAPI, { PLATFORM_ID: "euw1" });
  let league = null;

  const [summoner] = await getSummoner(accountName, accountId);
  if (summoner) {
    try {
      league = await handler.League.gettingEntriesForSummonerId(accountId);
    } catch (err) {
      logger.log("error", err);
    }

    await league.forEach(element => {
      switch (element.queueType) {
        case "RANKED_SOLO_5x5":
          summoner.update({ soloRank: `${element.tier} ${element.rank}` });
          break;
        case "RANKED_FLEX_SR":
          summoner.update({ flexRank: `${element.tier} ${element.rank}` });
          break;
        default:
          break;
      }
    });
    summoner.save();
  }
}

/*
  EXPORTS
*/
module.exports = { getSummoner, updateRank };
