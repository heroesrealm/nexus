/*
  REQUIREMENTS
*/
const { Tokens } = require("../data/initModels");

/*
  FUNCTIONS
*/
async function getToken(user) {
  return Tokens.findOne({ where: { userId: user.get("id") } });
}

async function createToken(count, user) {
  const token = await Tokens.create({ count });
  token.setUser(user);
}

async function addToken(token, count) {
  const actualTokens = token.get("count");
  token.update({ count: actualTokens + count });
}

/*
  EXPORTS
*/
module.exports = { getToken, addToken, createToken };
