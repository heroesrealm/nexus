/*
  REQUIREMENTS
*/
const { Settings } = require("../data/initModels");

/*
  PROPERTIES
*/
let serverID = null;

/*
  FUNCTIONS
*/
function getSettings(id) {
  if (id) serverID = id;
  return Settings.findOne({ where: { serverID } });
}

/*
  EXPORTS
*/
module.exports = { getSettings };
