/* eslint-disable no-restricted-syntax */
/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
/*
 * REQUIREMENTS
 */
const path = require("path");
const fs = require("fs");
const getLogger = require("../functions/getLogger");
const SettingsManager = require("../../managers/SettingsManager");

/*
 * PROPERTIES
 */
const logger = getLogger();
let handler = null;
const staffCommands = new Map();
const commands = new Map();
const aliases = new Map();

/*
 * UTILS
 */
var walk = function(dir, done) {
  let results = [];
  fs.readdir(dir, function(err, list) {
    if (err) return done(err);

    let pending = list.length;
    if (!pending) return done(null, results);

    list.forEach(function(file) {
      file = path.resolve(dir, file);
      fs.stat(file, function(err, stat) {
        if (err) logger.log("error", err);
        if (stat && stat.isDirectory()) {
          walk(file, function(err, res) {
            if (err) logger.log("error", err);
            results = results.concat(res);
            if (!--pending) done(null, results);
          });
        } else {
          results.push(file);
          if (!--pending) done(null, results);
        }
      });
    });
  });
};

/*
 * CLASS
 */
class CommandHandler {
  constructor(_data = {}) {
    const data = _data;
    if (!data.folder) throw new Error("No folder specified.");
    this.folder = data.folder;

    if (!data.prefix) throw new Error("No prefix specified.");

    if (!Array.isArray(data.prefix)) data.prefix = [data.prefix];
    data.prefix.sort((a, b) => a.length < b.length);
    this.prefix = data.prefix;

    this.loadFrom();
  }

  loadFrom() {
    const { folder } = this;
    walk(folder, function parseFiles(err, files) {
      if (err) throw err;

      const jsFiles = files.filter(f => f.endsWith(".js"));

      if (files.lenght <= 0) throw new Error("No commands to load!");
      const fileAmount = `${jsFiles.length}`;
      logger.log("info", `Found ${fileAmount} files to load!`);

      jsFiles.forEach(f => {
        const File = require(f);
        const cmd = new File();

        const name = cmd.name.toLowerCase();
        if (cmd.onlyStaff) staffCommands.set(name, cmd);
        else commands.set(name, cmd);

        logger.log("info", `Loading command: '${name}'.`);
        cmd.alias.forEach(alias => {
          aliases.set(alias, cmd);
        });
      });

      logger.log("info", "Done loading commands.");
    });
  }

  getCommand(string) {
    if (!string) return null;
    const commandString = string.toLowerCase();

    let prefix = "";
    let prefixExists = false;

    for (const x of this.prefix) {
      if (commandString.startsWith(x)) {
        prefix = x;
        prefixExists = true;
        break;
      }
    }

    if (!prefixExists) return null;

    const command = string.substring(prefix.length);
    let cmd = commands.get(command);
    if (!cmd) {
      cmd = staffCommands.get(command);
      if (!cmd) {
        const alias = aliases.get(command);
        if (!alias) return null;
        cmd = commands.get(alias.name);
      }
    }

    return cmd;
  }

  getCommands() {
    return commands;
  }
}

/*
 * FUNCTIONS
 */
async function getCommandHandler(serverID) {
  const settings = await SettingsManager.getSettings(serverID);
  if (handler === null) {
    handler = new CommandHandler({
      folder: path.join(__dirname, "../../commands/"),
      prefix: settings.get("prefix")
    });
  }
  return handler;
}

/*
 * EXPORTS
 */
module.exports = { CommandHandler, getCommandHandler };
