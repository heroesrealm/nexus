/* eslint-disable no-restricted-syntax */
/* eslint-disable no-shadow */
const { createLogger, format, transports } = require("winston");

const { combine, timestamp, colorize, printf } = format;

const StringBuilder = require("string-builder");

const logger = createLogger({
  transports: [new transports.Console()],
  format: combine(
    colorize(),
    timestamp(),
    printf(info => {
      const { timestamp, level, message, ...extra } = info;
      const ts = timestamp.slice(0, 19).replace("T", " ");
      const sb = new StringBuilder();

      if (message) {
        sb.append(`${ts} [${level}]: ${message}`);
      } else {
        for (const element in extra) {
          if (extra[element])
            sb.appendLine(`${ts} [${level}]: ${extra[element]}`);
        }
      }

      return sb.toString();
    })
  )
});

module.exports = () => {
  return logger;
};
