/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const RiotHandler = require("leaguejs");
const UserManager = require("../../managers/UserManager");
const BattleTagManager = require("../../managers/BattleTagManager");
const SummonerManager = require("../../managers/SummonerManager");
const owHandler = require("../../utils/classes/OverwatchHandler");
const { riotAPI } = require("../../settings.json");

/*
  COMMANDS
*/
class account {
  constructor() {
    this.name = "account";
    this.alias = ["accounts"];
    this.description = `Permet de connecter un compte Blizzard ou Riot.`;
    this.usage = "account [ow/lol] [compte]";
    this.onlyStaff = false;
  }

  async run(client, msg, args) {
    const { member } = msg;

    const embed = new Discord.RichEmbed()
      .setTitle(member.displayName)
      .setFooter(
        `${member.displayName}#${member.user.discriminator}`,
        member.user.avatarURL
      );

    let [description, color] = [null, null];
    if (args.length >= 3) {
      switch (args[1].toLowerCase()) {
        case "ow":
          [description, color] = await addBtag(msg, member, args[2]);
          break;
        case "lol":
          [description, color] = await addLoL(msg, member, args[2]);
          break;
        default:
          [description, color] = [
            "Le type de compte entré est inconnu.",
            "RED"
          ];
      }
    } else {
      [description, color] = [
        "Vous devez précisier le type de compte et le nom de compte.",
        "RED"
      ];
    }

    embed.setDescription(description).setColor(color);
    msg.channel.send(embed);
  }
}

const addBtag = async (msg, member, btag) => {
  if (!btag)
    return [
      `Vous devez entrer le BattleTag que vous souhaitez ajouter à votre compte.`,
      "RED"
    ];

  const [user] = await UserManager.getUser(member.id);
  if (user) {
    const search = await owHandler.search(btag);
    if (search.length !== 1)
      return [`Ce BattleTag n'a pas pu être identifié.`, "RED"];
    if (!search[0].isPublic)
      return [
        `Le profil lié à ce BattleTag est privé.\r\nMerci de le passer en public avant de refaire cette commande.`,
        "RED"
      ];
    if (search[0].platform !== "pc")
      return [
        `Ce BattleTag n'est pas lié à un profile présent sur PC.\r\nHeroes Realm ne prends en compte que les profils PC.`,
        "RED"
      ];

    const data = await owHandler.getOverall("pc", "eu", search[0].urlName);
    if (!data)
      return [
        `Le profil lié à ce BattleTag n'a pas pu être identifié.\r\nMerci de contacter un membre du Staff.`,
        "RED"
      ];

    const [battletag, created] = await BattleTagManager.getBattleTag(btag);
    if (created) {
      await battletag.setUser(user);
      await BattleTagManager.updateSR(btag);
      return [`Ce BattleTag a bien été ajouté à votre compte.`, "GREEN"];
    }
    const btagUser = battletag.getUser();

    if (btagUser.get("discordID") === user.get("discordID"))
      return [`Ce BattleTag est déjà lié à votre compte.`, "GOLD"];
    return [`Ce BattleTag est déjà lié à un autre compte.`, "RED"];
  }
  return undefined;
};

const addLoL = async (msg, member, lolAccount) => {
  if (!lolAccount)
    return [
      `Vous devez entrer le nom de profil Riot que vous souhaitez ajouter à votre compte.`,
      "RED"
    ];

  const handler = new RiotHandler(riotAPI, { PLATFORM_ID: "euw1" });
  let profile = null;
  let league = null;

  try {
    profile = await handler.Summoner.gettingByName(lolAccount);
  } catch (err) {
    return [`Le nom de profil n'a pas pu être identifié`, "RED"];
  }

  try {
    league = await handler.League.gettingEntriesForSummonerId(profile.id);
  } catch (err) {
    return [
      "Il y a eu un soucis lors de la récupération de vos informations.\r\nMerci de contacter un membre du Staff.",
      "RED"
    ];
  }

  if (league.length === 0) {
    return [
      `Votre compte n'est pas rank.\r\nRéessayer une fois que celui-ci sera placé.`,
      "GOLD"
    ];
  }

  const [user] = await UserManager.getUser(member.id);
  if (user) {
    const [summoner, created] = await SummonerManager.getSummoner(
      profile.name,
      profile.id
    );
    if (created) {
      await summoner.setUser(user);
      await SummonerManager.updateRank(profile.id, profile.name);
      return [
        `Ce profil d'invocateur a bien été ajouté à votre compte.`,
        "GREEN"
      ];
    }
    const summonerUser = await SummonerManager.getSummonerUser(summoner.name);

    if (summonerUser.get("discordID") === user.get("discordID"))
      return [`Ce profil d'invocateur est déjà lié à votre compte.`, "GOLD"];
    return [`Ce profil d'invocateur est déjà lié à un autre compte.`, "RED"];
  }
  return undefined;
};

/*
  EXPORTS
*/
module.exports = account;
