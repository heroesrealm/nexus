/* eslint-disable no-control-regex */
/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const net = require("net");

const embed = new Discord.RichEmbed();

/*
  COMMANDS
*/
class minecraft {
  constructor() {
    this.name = "minecraft";
    this.alias = ["mc", "mcserv", "ftb"];
    this.description = "Permet d'obtenir des infos sur le serveur minecraft.";
    this.usage = "minecraft";
    this.onlyStaff = false;
  }

  async run(client, msg, args) {
    const { member } = msg;
    const StartTime = new Date();
    this.port = 13056;
    this.address = "heroes-realm.omgcraft.fr";
    const clientMC = net.connect(this.port, this.address, () => {
      this.latency = Math.round(new Date() - StartTime);
      const buff = Buffer.from([0xfe, 0x01]);
      clientMC.write(buff);
    });

    clientMC.setTimeout(5 * 1000);

    clientMC.on("error", err => {
      let msg2 = "";
      if (err.code === "ENOTFOUND") {
        msg2 = `Impossible de trouver l'ip ${this.address}.`;
        return;
      }
      if (err.code === "ECONNREFUSED") {
        msg2 = `Impossible de me connecter au port ${this.port}.`;
      }
      embed
        .setTitle("Etat serveur minecraft :")
        .setFooter(
          `${member.displayName}#${member.user.discriminator}`,
          member.user.avatarURL
        );
      embed.setDescription(msg2).setColor("RED");
      msg.channel.send(embed);
    });

    clientMC.on("timeout", () => {
      embed
        .setTitle("Etat serveur minecraft :")
        .setFooter(
          `${member.displayName}#${member.user.discriminator}`,
          member.user.avatarURL
        );
      embed.setDescription("TIMEOUT LE SERV EST MORT").setColor("RED");
      msg.channel.send(embed);
      clientMC.end();
      process.exit();
    });

    clientMC.on("data", data => {
      if (data != null && data !== "") {
        const serverInfo = data.toString().split("\x00\x00\x00");
        if (serverInfo != null && serverInfo.length >= 6) {
          this.online = true;
          this.version = serverInfo[2].replace(/\u0000/g, "");
          this.motd = serverInfo[3].replace(/\u0000/g, "");
          this.current_players = serverInfo[4].replace(/\u0000/g, "");
          this.max_players = serverInfo[5].replace(/\u0000/g, "");
          embed
            .setTitle("Etat serveur minecraft :")
            .setFooter(
              `${member.displayName}#${member.user.discriminator}`,
              member.user.avatarURL
            );
          embed
            .setDescription(
              `Online\nNombres de joueurs connectés :${this.current_players}/${this.max_players}`
            )
            .setColor("GREEN");
          msg.channel.send(embed);
        } else {
          this.online = false;
          embed
            .setTitle("Etat serveur minecraft :")
            .setFooter(
              `${member.displayName}#${member.user.discriminator}`,
              member.user.avatarURL
            );
          embed
            .setDescription(
              "Il est en ligne mais je n'arrive pas a obtenir les informations, peux etre un redemarrage"
            )
            .setColor("RED");
          msg.channel.send(embed);
        }
      }
      clientMC.end();
    });
  }
}

/*
  EXPORTS
*/
module.exports = minecraft;
