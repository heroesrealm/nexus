/*
  REQUIREMENTS
*/
const async = require("async");

const moment = require(`moment`);
const Discord = require(`discord.js`);
const UserManager = require(`../../managers/UserManager`);
const FactionManager = require(`../../managers/FactionManager`);
const ExperienceManager = require(`../../managers/ExperienceManager`);
const SeasonManager = require(`../../managers/SeasonManager`);
const StringBuilder = require(`string-builder`);
const getLogger = require(`../../utils/functions/getLogger`);

/*
  PROPERTIES
*/
const logger = getLogger();
let factionCheck = "";

/*
  COMMANDS
*/
class info {
  constructor() {
    this.name = `info`;
    this.alias = [`infos`, `information`, `informations`];
    this.description = "Donne les informations d`un utilisateur.";
    this.usage = `info [member]`;
    this.onlyStaff = false;
  }

  async run(client, msg, args) {
    if (args.length > 1) {
      args.shift();
      async.eachSeries(
        args,
        async.asyncify(async arg => {
          if (arg.startsWith("<@") && arg.endsWith(">")) {
            let mention = arg.slice(2, -1);
            if (mention.startsWith("!")) mention = mention.slice(1);

            handleUserInfo(msg, msg.guild.members.get(mention));
          } else {
            factionCheck += `${arg} `;
            const faction = await FactionManager.getFaction(
              factionCheck.slice(0, -1)
            );
            if (faction) handleFactionInfo(msg, faction);
          }
        }),
        err => {
          if (err) logger.log("error", err);
        }
      );
    } else {
      handleUserInfo(msg, msg.member);
    }
  }
}

const handleUserInfo = async (msg, member) => {
  const [user] = await UserManager.getUser(member.id);
  const userHandler = new UserInfoHandler(msg, member, user);
  userHandler.createInfo();
};

const UserInfoHandler = (msg, member, user) => {
  this.msg = msg;
  this.member = member;
  this.user = user;

  this.createInfo = async () => {
    const embed = new Discord.RichEmbed()
      .setColor(`BLUE`)
      .setFooter(
        `${msg.member.displayName}#${msg.member.user.discriminator}`,
        msg.member.user.avatarURL
      );

    async.parallel(
      [
        this.updateData,
        this.handleFaction,
        this.handleBattleTags,
        this.handleSummoners,
        this.handleSeasons,
        this.handleTokens,
        this.handleOthers
      ],
      (err, result) => {
        if (err) throw logger.log(`error`, err);

        result.forEach(element => {
          if (element) embed.addField(element[0], element[1]);
        });

        embed.setTitle(member.displayName).setThumbnail(member.user.avatarURL);
        msg.channel.send(embed);
      }
    );
  };

  this.updateData = async () => {
    UserManager.updateData(member);
    return null;
  };

  this.handleFaction = async () => {
    const faction = await user.getFaction();
    if (faction) {
      const dateJoinFaction = user.get(`dateJoinFaction`);
      return [
        `🛡️ Faction`,
        `Dans la faction **${faction.get(`name`)}** depuis ${moment(
          dateJoinFaction
        )
          .locale(`fr`)
          .fromNow(true)}`
      ];
    }
    return null;
  };

  this.handleBattleTags = async () => {
    const battletags = await user.getBattletags();
    if (battletags) {
      const sb = new StringBuilder();
      battletags.forEach(battletag => {
        sb.appendLine(`${battletag.get(`battletag`)}`);
        if (battletag.get(`skillRating`) !== -1)
          sb.append(` [**${battletag.get(`skillRating`)}**]`);
      });

      if (sb.toString()) return [`⚔️ BattleTag`, sb.toString()];
    } else return null;
  };

  this.handleSummoners = async () => {
    const summoners = await user.getSummoners();
    if (summoners) {
      const sb = new StringBuilder();
      summoners.forEach(summoner => {
        sb.appendLine(`${summoner.get(`accountName`)}`);
        if (summoner.get(`soloRank`))
          sb.append(` [Solo : **${summoner.get(`soloRank`)}**]`);
        if (summoner.get(`flexRank`))
          sb.append(` [Flex : **${summoner.get(`flexRank`)}**]`);
      });

      if (sb.toString()) return [`⚔️ Profil d'Invocateur`, sb.toString()];
    } else return null;
  };

  this.handleSeasons = async () => {
    const actualSeason = await SeasonManager.getActualSeason();
    const actualExperiences = await ExperienceManager.getActualExperiences(
      user
    );

    if (actualExperiences) {
      const sb = new StringBuilder();
      const [xp, level] = await ExperienceManager.getLevelByXP(
        actualExperiences.get(`count`)
      );
      const xpLimit = await ExperienceManager.getXPLimitByLevel(level);

      sb.appendLine(`Niveau : **${level}**`);
      sb.appendLine(`Expérience : **${xp}/${xpLimit}**`);
      if (sb.toString()) return [`🏆 Saison ${actualSeason}`, sb.toString()];
    } else return null;
  };

  this.handleTokens = async () => {
    const tokens = await user.getTokens();
    if (tokens.length > 0)
      return [`💰 Tokens`, `Nombre de tokens : **${tokens[0].get(`count`)}**`];
    return null;
  };

  this.handleOthers = async () => {
    return [
      `🏷️ Autres`,
      `Sur le serveur depuis ${moment(member.joinedAt)
        .locale(`fr`)
        .fromNow(true)}`
    ];
  };
};

async function handleFactionInfo(msg, faction) {
  factionCheck = "";
  const factionHandler = new FactionInfoHandler(msg, faction);
  factionHandler.createInfo(err => {
    if (err) logger.log("error", err);
    return true;
  });
}

const FactionInfoHandler = (msg, faction) => {
  this.msg = msg;
  this.faction = faction;

  this.createInfo = async callback => {
    const embed = new Discord.RichEmbed()
      .setColor(`BLUE`)
      .setFooter(
        `${msg.member.displayName}#${msg.member.user.discriminator}`,
        msg.member.user.avatarURL
      );

    async.parallel([this.handleUsersCount], (err, result) => {
      if (err) throw logger.log(`error`, err);

      result.forEach(element => {
        if (element) embed.addField(element[0], element[1]);
      });

      embed.setTitle(faction.get("name"));
      msg.channel.send(embed);
      callback(null);
    });
  };

  this.handleUsersCount = async () => {
    const users = await faction.getUsers();

    const sb = new StringBuilder();

    const roleStaffName = `Responsable ${faction.get("name")}`;
    const roleStaff = msg.guild.roles.find(
      r =>
        r.name.toLowerCase().normalize("NFD") ===
        roleStaffName.toLowerCase().normalize("NFD")
    );
    const staff = roleStaff.members.first();
    if (staff) sb.appendLine(`**Reponsable de Faction :** ${staff}`);

    const roleChefName = `Chef ${faction.get("name")}`;
    const roleChef = msg.guild.roles.find(
      r =>
        r.name.toLowerCase().normalize("NFD") ===
        roleChefName.toLowerCase().normalize("NFD")
    );
    const chef = roleChef.members.first();
    if (chef) sb.appendLine(`**Chef de Faction :** ${chef}`);

    sb.appendLine(`**Nombre de membre :** ${users.length}`);

    return ["🛡️ Effectif", sb.toString()];
  };
};

/*
  EXPORTS
*/
module.exports = info;
