/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const moment = require("moment");

/*
  COMMANDS
*/
class ping {
  constructor() {
    this.name = "ping";
    this.alias = ["pong"];
    this.description = "Retourne le ping du bot.";
    this.usage = "ping";
    this.onlyStaff = false;
  }

  run(client, msg, args) {
    const pingValue = moment().diff(msg.createdTimestamp);

    const embed = new Discord.RichEmbed()
      .setColor("BLUE")
      .setTitle("Ping")
      .setDescription(`${pingValue}ms`)
      .setFooter(
        `${msg.member.displayName}#${msg.member.user.discriminator}`,
        msg.member.user.avatarURL
      );

    msg.channel.send(embed);
  }
}

/*
  EXPORTS
*/
module.exports = ping;
