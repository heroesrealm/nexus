/*
  REQUIREMENTS
*/
const async = require("async");
const Discord = require("discord.js");
const FactionManager = require("../../managers/FactionManager");
const ExperienceManager = require("../../managers/ExperienceManager");
const SeasonManager = require("../../managers/SeasonManager");
const getLogger = require("../../utils/functions/getLogger");

const logger = getLogger();

/*
  COMMANDS
*/
class top {
  constructor() {
    this.name = "top";
    this.alias = ["topFaction", "retopdre", "retopdreFaction"];
    this.description = "Permet de retopdre la faction de son choix.";
    this.usage = "top [faction]";
    this.onlyStaff = false;
  }

  async run(client, msg, args) {
    const { member } = msg;

    let factionName = args[1];
    for (let index = 2; index < args.length; index += 1) {
      factionName += ` ${args[index]}`;
    }

    const embed = await topFaction(msg, member, factionName);

    msg.channel.send(embed);
  }
}

async function topFaction(msg, member, factionName) {
  let embed = new Discord.RichEmbed()
    .setColor(`BLUE`)
    .setFooter(
      `${msg.member.displayName}#${msg.member.user.discriminator}`,
      msg.member.user.avatarURL
    );

  const actualSeason = await SeasonManager.getActualSeason();
  if (!actualSeason)
    return embed.setDescription(`Aucun saison n'est en cours.`).setColor("RED");

  if (!factionName)
    return embed
      .setDescription(`Vous devez entrer le nom d'une faction.`)
      .setColor("RED");

  const faction = await FactionManager.getFaction(factionName);
  if (faction) {
    const users = await faction.getUsers();
    if (users.length === 0)
      return embed.setDescription("La faction est vide.").setColor("RED");

    const xpMap = await getXPMap(users);
    xpMap[Symbol.iterator] = function* iterate() {
      yield* [...this.entries()].sort((a, b) => b[1] - a[1]);
    };

    embed = await formatEmbed(embed, xpMap, msg);
    embed.setTitle(`Classement ${faction.get("name")}`);

    return embed;
  }
  return embed
    .setDescription(`La faction ${factionName} n'existe pas.`)
    .setColor("RED");
}

async function getXPMap(users) {
  return new Promise((resolve, reject) => {
    const xpMap = new Map();
    async.eachLimit(
      users,
      10,
      async.asyncify(async user => {
        const actualXP = await ExperienceManager.getActualExperiences(user);
        if (actualXP) xpMap.set(user.get("discordID"), actualXP.get("count"));
      }),
      err => {
        if (err) logger.log("error", err);
        resolve(xpMap);
      }
    );
  });
}

async function formatEmbed(embed, xpMap, msg) {
  return new Promise((resolve, reject) => {
    let i = 0;
    async.eachSeries(
      xpMap,
      async.asyncify(async xp => {
        i += 1;
        const xpMember = msg.guild.members.get(xp[0]);
        const xpCount = xp[1];
        const [realXP, realLevel] = await ExperienceManager.getLevelByXP(
          xpCount
        );
        const xpLimit = await ExperienceManager.getXPLimitByLevel(realLevel);

        let emoji = "";
        switch (i) {
          case 1:
            emoji = "🥇 ";
            break;
          case 2:
            emoji = "🥈 ";
            break;
          case 3:
            emoji = "🥉 ";
            break;
          default:
            break;
        }

        embed.addField(
          `${emoji}**${xpMember.displayName}**`,
          `Niveau **${realLevel}** [**${realXP}/${xpLimit}**]`
        );
      }),
      err => {
        if (err) logger.log("error", err);
        resolve(embed);
      }
    );
  });
}

/*
  EXPORTS
*/
module.exports = top;
