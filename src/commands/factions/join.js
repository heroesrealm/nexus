/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const UserManager = require("../../managers/UserManager");
const FactionManager = require("../../managers/FactionManager");

/*
  COMMANDS
*/
class join {
  constructor() {
    this.name = "join";
    this.alias = ["joinFaction", "rejoindre", "rejoindreFaction"];
    this.description = "Permet de rejoindre la faction de son choix.";
    this.usage = "join [faction]";
    this.onlyStaff = false;
  }

  async run(client, msg, args) {
    const { member } = msg;

    const embed = new Discord.RichEmbed()
      .setTitle(member.displayName)
      .setFooter(
        `${member.displayName}#${member.user.discriminator}`,
        member.user.avatarURL
      );
    let factionName = args[1];
    for (let index = 2; index < args.length; index += 1) {
      factionName += ` ${args[index]}`;
    }
    const [description, color] = await joinFaction(msg, member, factionName);
    embed.setDescription(description).setColor(color);

    msg.channel.send(embed);
  }
}

async function joinFaction(msg, member, factionName) {
  if (!factionName)
    return [
      `Vous devez entrer le nom de la faction que vous souhaitez rejoindre.`,
      "RED"
    ];

  const [user] = await UserManager.getUser(member.id);
  if (user) {
    const userFaction = await user.getFaction();
    if (userFaction)
      return [
        `Vous devez d'abord quitter votre faction pour en changer.`,
        "RED"
      ];

    const faction = await FactionManager.getFaction(factionName);
    if (faction) {
      const isJoinable = faction.get("isJoinable");
      if (!isJoinable)
        return [
          `La faction ${faction.get("name")} est fermée. Réessayer plus tard.`,
          "GOLD"
        ];

      UserManager.setFaction(member, faction.get("name"), msg.guild);
      return [
        `Vous avez rejoins la faction ${faction.get("name")} avec succès !`,
        "GREEN"
      ];
    }
    return [`La faction ${factionName} n'existe pas.`, "RED"];
  }
}

/*
  EXPORTS
*/
module.exports = join;
