/*
  REQUIREMENTS
*/
const moment = require("moment");
const Discord = require("discord.js");
const UserManager = require("../../managers/UserManager");
const SettingsManager = require("../../managers/SettingsManager");
const SeasonManager = require("../../managers/SeasonManager");

/*
  PROPERTIES
*/
const embed = new Discord.RichEmbed();

/*
  COMMANDS
*/
class join {
  constructor() {
    this.name = "leave";
    this.alias = ["leaveFaction", "quitter", "quitterFaction"];
    this.description = "Permet de quitter votre faction.";
    this.usage = "leave";
    this.onlyStaff = false;
  }

  async run(client, msg, args) {
    const { member } = msg;

    embed
      .setTitle(member.displayName)
      .setFooter(
        `${member.displayName}#${member.user.discriminator}`,
        member.user.avatarURL
      );

    const [description, color] = await leaveFaction(msg, member);
    embed.setDescription(description).setColor(color);

    msg.channel.send(embed);
  }
}

async function leaveFaction(msg, member) {
  const actualSeason = await SeasonManager.getActualSeason();

  if (actualSeason) {
    return [
      `Vous ne pouvez pas quitter votre faction en cours de saison.\r\nEn cas de soucis, merci de contactez votre Responsable de faction.`,
      "RED"
    ];
  }
  const [user] = await UserManager.getUser(member.id);
  if (user) {
    const settings = await SettingsManager.getSettings();
    const leaveCooldown = settings.get("leaveCooldown");

    const dateJoinFaction = user.get("dateJoinFaction");
    const timeInFaction = Math.abs(moment().diff(dateJoinFaction, "minutes"));

    const timeUntilLeave = leaveCooldown - timeInFaction;

    if (timeUntilLeave > 0)
      return [
        `Vous devez encore attendre ${timeUntilLeave} minutes avant de quitter votre faction.`,
        "RED"
      ];

    UserManager.removeFaction(member, msg.guild);
    return [`Vous avez quitté votre faction avec succès !`, "GREEN"];
  }
}

/*
  EXPORTS
*/
module.exports = join;
