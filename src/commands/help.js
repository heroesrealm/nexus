/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const { getCommandHandler } = require("../utils/classes/CommandHandler");

/*
  COMMANDS
*/
class help {
  constructor() {
    this.name = "help";
    this.alias = ["aide"];
    this.description =
      "Permet d'afficher la liste des commandes disponibles et leurs utilisations.";
    this.usage = "help";
    this.onlyStaff = false;
  }

  async run(client, msg, args) {
    const embed = new Discord.RichEmbed()
      .setColor("BLUE")
      .setTitle("Nexus commands")
      .setFooter(
        `${msg.member.displayName}#${msg.member.user.discriminator}`,
        msg.member.user.avatarURL
      )
      .attachFile("./assets/img/help.png")
      .setThumbnail("attachment://help.png");

    const handler = await getCommandHandler();
    const commands = handler.getCommands();

    commands.forEach(cmd => {
      embed.addField(`${handler.prefix}${cmd.usage}`, `${cmd.description}`);
    });

    msg.channel.send(embed);
  }
}

/*
  EXPORTS
*/
module.exports = help;
