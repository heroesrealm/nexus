/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const SeasonManager = require("../../managers/SeasonManager");

/*
  COMMANDS
*/
class season {
  constructor() {
    this.name = "season";
    this.alias = [];
    this.description = "Permet de connaitre la saison actuelle.";
    this.usage = "season";
    this.onlyStaff = false;
  }

  async run(client, msg, args) {
    const [description, color] = await handleSeasons();

    const embed = new Discord.RichEmbed()
      .setFooter(
        `${msg.member.displayName}#${msg.member.user.discriminator}`,
        msg.member.user.avatarURL
      )
      .setDescription(description)
      .setColor(color);
    msg.channel.send(embed);
  }
}

async function handleSeasons() {
  const actualSeason = await SeasonManager.getActualSeason();

  if (actualSeason) {
    return [`La saison ${actualSeason} est en cours.`, "GOLD"];
  }
  return [`Aucune saison n'est en cours.`, "GOLD"];
}

/*
  EXPORTS
*/
module.exports = season;
