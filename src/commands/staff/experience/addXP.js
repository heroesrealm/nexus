/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const UserManager = require("../../../managers/UserManager");
const SeasonManager = require("../../../managers/SeasonManager");
const ExperienceManager = require("../../../managers/ExperienceManager");

/*
  COMMANDS
*/
class addXP {
  constructor() {
    this.name = "addXP";
    this.alias = [];
    this.onlyStaff = true;
  }

  async run(client, msg, args) {
    if (msg.member.hasPermission("ADMINISTRATOR", true, true, true)) {
      const [description, color] = await handleExperiences(msg, args);
      const embed = new Discord.RichEmbed()
        .setFooter(
          `${msg.member.displayName}#${msg.member.user.discriminator}`,
          msg.member.user.avatarURL
        )
        .setDescription(description)
        .setColor(color);
      msg.channel.send(embed);
    }
  }
}

async function handleExperiences(msg, args) {
  const actualSeason = await SeasonManager.getActualSeason();
  if (!actualSeason) return [`Aucun saison n'est en cours.`, "RED"];

  if (args.length >= 3) {
    const { members } = msg.mentions;
    if (members.array().length === 0)
      return [`Vous devez mentionner au moins un utilisateur.`, "RED"];

    const experiences = Number(args[args.length - 1]);
    if (Number.isNaN(experiences))
      return [
        `Le nombre de experiences doit être un nombre et doit être en dernier position.`,
        "RED"
      ];

    members.forEach(async member => {
      const [user] = await UserManager.getUser(member.id);
      let levelBefore;
      let levelAfter;
      let actualExperiences = await ExperienceManager.getActualExperiences(
        user
      );
      if (actualExperiences)
        [, levelBefore] = await ExperienceManager.getLevelByXP(
          actualExperiences.get(`count`)
        );

      await UserManager.addExperiences(member, experiences);

      actualExperiences = await ExperienceManager.getActualExperiences(user);
      if (actualExperiences)
        [, levelAfter] = await ExperienceManager.getLevelByXP(
          actualExperiences.get(`count`)
        );

      if (levelBefore !== levelAfter) {
        const embed = new Discord.RichEmbed()
          .setFooter(
            `${msg.member.displayName}#${msg.member.user.discriminator}`,
            msg.member.user.avatarURL
          )
          .setDescription(
            `✨ Félicitations <@!${member.id}> ! Tu viens de passé au **niveau ${levelAfter}**`
          )
          .setColor("GREEN");
        msg.channel.send(embed);
      }
    });

    if (members.array().length > 1)
      return [
        `Les points d'expérience des joueurs ont bien été modifiés.`,
        "GREEN"
      ];
    return [
      `Les points d'expérience du joueur ont bien été modifiés.`,
      "GREEN"
    ];
  }
  return [
    `Vous devez préciser le ou les utilisateur(s) et le nombre de points d'expériences à ajouter.`,
    "RED"
  ];
}

/*
  EXPORTS
*/
module.exports = addXP;
