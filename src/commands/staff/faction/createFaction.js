/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const { Factions } = require("../../../data/initModels");
const logger = require("../../../utils/functions/getLogger");

/*
  COMMANDS
*/
class createFaction {
  constructor() {
    this.name = "createFaction";
    this.alias = [];
    this.onlyStaff = true;
  }

  run(client, msg, args) {
    const { member } = msg;

    if (member.hasPermission("ADMINISTRATOR", true, true, true)) {
      const embed = new Discord.RichEmbed()
        .setTitle(member.displayName)
        .setFooter(
          `${member.displayName}#${member.user.discriminator}`,
          member.user.avatarURL
        );

      if (args.length > 1) {
        let factionName = args[1];
        for (let index = 2; index < args.length; index += 1) {
          factionName += ` ${args[index]}`;
        }
        Factions.upsert({ name: factionName });

        const guild = client.guilds.first();
        const roleName = factionName;
        if (!guild.roles.find(x => x.name === roleName)) {
          guild
            .createRole({ name: roleName })
            .catch(err => logger.log("error", err));
        }

        const chefName = `Chef ${factionName}`;
        if (!guild.roles.find(x => x.name === chefName)) {
          guild
            .createRole({ name: chefName })
            .catch(err => logger.log("error", err));
        }

        const responsableName = `Responsable ${factionName}`;
        if (!guild.roles.find(x => x.name === responsableName)) {
          guild
            .createRole({ name: responsableName })
            .catch(err => logger.log("error", err));
        }

        embed
          .setColor("GREEN")
          .setDescription(`Vous avez bien créé la faction ${factionName}`);
      } else {
        embed
          .setColor("RED")
          .setDescription("Veuillez entrer le nom de la faction à créer.");
      }
      msg.channel.send(embed);
    }
  }
}

/*
  EXPORTS
*/
module.exports = createFaction;
