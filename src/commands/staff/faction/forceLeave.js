/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const UserManager = require("../../../managers/UserManager");

/*
  COMMANDS
*/
class forceLeave {
  constructor() {
    this.name = "forceLeave";
    this.alias = [];
    this.onlyStaff = true;
  }

  async run(client, msg, args) {
    if (msg.member.hasPermission("ADMINISTRATOR", true, true, true)) {
      const embed = new Discord.RichEmbed()
        .setTitle(msg.member.displayName)
        .setFooter(
          `${msg.member.displayName}#${msg.member.user.discriminator}`,
          msg.member.user.avatarURL
        );

      const member = msg.mentions.members.first();
      if (!member) {
        embed
          .setColor("RED")
          .setDescription("Vous devez mentionner un utilisateur.");
      } else {
        UserManager.removeFaction(member, msg.guild);
        embed
          .setColor("GREEN")
          .setDescription(
            `${member.displayName} a été forcé à quitter sa faction avec succès !`
          );
      }
      msg.channel.send(embed);
    }
  }
}

/*
  EXPORTS
*/
module.exports = forceLeave;
