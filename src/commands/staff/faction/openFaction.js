/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const FactionManager = require("../../../managers/FactionManager");

/*
  COMMANDS
*/
class openFaction {
  constructor() {
    this.name = "openFaction";
    this.alias = [];
    this.onlyStaff = true;
  }

  async run(client, msg, args) {
    const { member } = msg;

    if (member.hasPermission("ADMINISTRATOR", true, true, true)) {
      const embed = new Discord.RichEmbed()
        .setTitle(member.displayName)
        .setFooter(
          `${member.displayName}#${member.user.discriminator}`,
          member.user.avatarURL
        );

      if (args.length > 1) {
        let factionName = args[1];
        for (let index = 2; index < args.length; index += 1) {
          factionName += ` ${args[index]}`;
        }
        const faction = await FactionManager.getFaction(factionName);
        if (faction) {
          faction.update({ isJoinable: 1 });

          embed
            .setColor("GREEN")
            .setDescription(
              `La faction ${faction.get("name")} a été ouverte avec succès.`
            );
        } else {
          embed
            .setColor("RED")
            .setDescription(`La faction ${faction.get("name")} n'existe pas.`);
        }
      } else {
        embed
          .setColor("RED")
          .setDescription("Veuillez entrer le nom de la faction à ouvrir.");
      }
      msg.channel.send(embed);
    }
  }
}

/*
  EXPORTS
*/
module.exports = openFaction;
