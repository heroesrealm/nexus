/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const SeasonManager = require("../../../managers/SeasonManager");

/*
  COMMANDS
*/
class endSeason {
  constructor() {
    this.name = "endSeason";
    this.alias = [];
    this.onlyStaff = true;
  }

  async run(client, msg, args) {
    if (msg.member.hasPermission("ADMINISTRATOR", true, true, true)) {
      const [description, color] = await handleSeasons();

      const embed = new Discord.RichEmbed()
        .setFooter(
          `${msg.member.displayName}#${msg.member.user.discriminator}`,
          msg.member.user.avatarURL
        )
        .setDescription(description)
        .setColor(color);
      msg.channel.send(embed);
    }
  }
}

async function handleSeasons() {
  const actualSeason = await SeasonManager.getActualSeason();

  if (actualSeason) {
    SeasonManager.endSeason(actualSeason);
    return [`La saison ${actualSeason} viens de se terminer.`, "GREEN"];
  }
  return [`Aucun saison n'est en cours.`, "RED"];
}

/*
  EXPORTS
*/
module.exports = endSeason;
