/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const SeasonManager = require("../../../managers/SeasonManager");

/*
  COMMANDS
*/
class nextSeason {
  constructor() {
    this.name = "nextSeason";
    this.alias = [];
    this.onlyStaff = true;
  }

  async run(client, msg, args) {
    if (msg.member.hasPermission("ADMINISTRATOR", true, true, true)) {
      const [description, color] = await handleSeasons();

      const embed = new Discord.RichEmbed()
        .setFooter(
          `${msg.member.displayName}#${msg.member.user.discriminator}`,
          msg.member.user.avatarURL
        )
        .setDescription(description)
        .setColor(color);
      msg.channel.send(embed);
    }
  }
}

async function handleSeasons() {
  const actualSeason = await SeasonManager.getActualSeason();

  if (actualSeason) {
    SeasonManager.endSeason(actualSeason);
    SeasonManager.startSeason(actualSeason + 1);
    return [`La saison ${actualSeason + 1} viens de commencer.`, "GREEN"];
  }
  const lastSeason = await SeasonManager.getLastSeason();

  if (lastSeason) {
    SeasonManager.startSeason(lastSeason + 1);
    return [`La saison ${lastSeason + 1} viens de commencer.`, "GREEN"];
  }
  SeasonManager.startSeason(1);
  return ["La saison 1 viens de commencer.", "GREEN"];
}

/*
  EXPORTS
*/
module.exports = nextSeason;
