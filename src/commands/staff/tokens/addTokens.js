/*
  REQUIREMENTS
*/
const Discord = require("discord.js");
const UserManager = require("../../../managers/UserManager");

/*
  COMMANDS
*/
class addTokens {
  constructor() {
    this.name = "addTokens";
    this.alias = [];
    this.onlyStaff = true;
  }

  async run(client, msg, args) {
    if (msg.member.hasPermission("ADMINISTRATOR", true, true, true)) {
      const [description, color] = await handleTokens(msg, args);
      const embed = new Discord.RichEmbed()
        .setFooter(
          `${msg.member.displayName}#${msg.member.user.discriminator}`,
          msg.member.user.avatarURL
        )
        .setDescription(description)
        .setColor(color);
      msg.channel.send(embed);
    }
  }
}

async function handleTokens(msg, args) {
  if (args.length >= 3) {
    const { members } = msg.mentions;
    if (members.array().length === 0)
      return [`Vous devez mentionner au moins un utilisateur.`, "RED"];

    const experiences = Number(args[args.length - 1]);
    if (Number.isNaN(experiences))
      return [
        `Le nombre de tokens doit être un nombre et doit être en dernier position.`,
        "RED"
      ];

    members.forEach(async member => {
      await UserManager.addTokens(member, experiences);
    });

    if (members.array().length > 1)
      return [`Les tokens des joueurs ont bien été modifiés.`, "GREEN"];
    return [`Les tokens du joueur ont bien été modifiés.`, "GREEN"];
  }
  return [
    `Vous devez préciser l'utilisateur et le nombre de tokens à ajouter.`,
    "RED"
  ];
}

/*
  EXPORTS
*/
module.exports = addTokens;
