/*
  REQUIREMENTS
*/
const async = require("async");
const moment = require("moment");
const Discord = require("discord.js");
const { Client } = require("discord.js");
const { getCommandHandler } = require("./utils/classes/CommandHandler");
const { token } = require("./settings.json");
const getLogger = require("./utils/functions/getLogger");
const initDatabase = require("./data/initDatabase");
const { initModels } = require("./data/initModels");
const SettingsManager = require("./managers/SettingsManager");

/*
  PROPERTIES
*/
const logger = getLogger();

const client = new Client();
let handler = null;

/*
  FUNCTIONS
*/
async function initHandler(serverID) {
  handler = await getCommandHandler(serverID);
}

/*
  EVENTS
*/
client.on("ready", async () => {
  logger.log("info", `${client.user.username} is ready!`);
  client.user.setActivity("you all, humans.", { type: "WATCHING" });

  moment.tz.setDefault("Europe/Paris");

  async.series([initDatabase, initModels], (err, result) => {
    if (err) throw logger.log("error", err);
    logger.log("info", result);
    initHandler(client.guilds.first().id);
  });
});

client.on("message", msg => {
  if (msg.channel.type === "dm") return;
  if (msg.author.type === "bot") return;

  const message = msg; // * follow eslint no-param-reassign rule
  message.content = msg.content.replace(/  +/g, " ");
  const args = message.content.split(" ");
  const command = handler.getCommand(args[0]);

  if (!command) return;

  try {
    logger.log(
      "info",
      `${msg.member.user.tag}(${msg.member.user.id}) is calling ${command.name} command.`
    );
    logger.log("info", ` => [${msg}]`);
    command.run(client, msg, args);
  } catch (err) {
    logger.log("error", err);
  }
});

client.on("guildMemberAdd", async member => {
  const guildID = client.guilds.first().id;
  const settings = await SettingsManager.getSettings(guildID);

  // Add Heroes role
  const role = client.guilds.first().roles.find(r => r.name === "Héros");
  member.addRole(role).catch(err => logger.log("error", err));

  // Send dm
  const message = settings.get("joinMessage");

  const embed = new Discord.RichEmbed()
    .setColor("BLUE")
    .setDescription(message);
  member.send("", embed);

  // Send join message
  const channelID = settings.get("joinChannel");
  const channel = member.guild.channels.get(channelID);

  channel.send(
    `${member} (${member.user.username}) viens de rejoindre le serveur.`
  );
});

client.on("guildMemberRemove", async member => {
  // Send leave message
  const guildID = client.guilds.first().id;
  const settings = await SettingsManager.getSettings(guildID);

  const channelID = settings.get("joinChannel");
  const channel = member.guild.channels.get(channelID);

  const diff = Math.abs(moment().diff(member.joinedAt, "minutes"));
  moment.locale("fr");

  channel.send(
    `${member} (${
      member.user.username
    }) viens de quitter le serveur après ${moment
      .duration(diff, "minutes")
      .humanize()}.`
  );
});

process.on("uncaughtException", error => logger.log("error", error));

client.login(token);
