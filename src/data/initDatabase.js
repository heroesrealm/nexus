/*
  REQUIREMENTS
*/
const async = require("async");
const Sequelize = require("sequelize");
const {
  database,
  host,
  user,
  password,
  dialect,
  logging
} = require("../settings.json");
const getLogger = require("../utils/functions/getLogger");

/*
  PROPERTIES
*/
const logger = getLogger();
// On met '' en base de données pour pouvoir la créer si besoin
// sequelize sera réinstancié avec plus tard
let sequelize = new Sequelize("", user, password, {
  host,
  dialect,
  logging,
  dialectOptions: {
    useUTC: false // * for reading from database
  },
  timezone: "+02:00" // * for writing to database
});

/*
  FUNCTIONS
*/
function createDatabase(callback) {
  sequelize.query(`CREATE DATABASE IF NOT EXISTS ${database};`).then(() => {
    sequelize = new Sequelize(database, user, password, {
      host,
      dialect,
      logging,
      dialectOptions: {
        useUTC: false // * for reading from database
      },
      timezone: "+02:00" // * for writing to database
    });

    callback(null, `Done creating database ${database}.`);
  });
}

function syncModels(callback) {
  const Settings = sequelize.import("models/Settings");
  const Factions = sequelize.import("models/Factions");
  const Users = sequelize.import("models/Users");
  const BattleTags = sequelize.import("models/BattleTags");
  const Summoners = sequelize.import("models/Summoners.js");
  const Experiences = sequelize.import("models/Experiences");
  const Seasons = sequelize.import("models/Seasons");
  const Tokens = sequelize.import("models/Tokens");

  Factions.hasMany(Users);
  Users.belongsTo(Factions);

  Users.hasMany(BattleTags);
  BattleTags.belongsTo(Users);

  Users.hasMany(Summoners);
  Summoners.belongsTo(Users);

  Users.hasMany(Experiences);
  Experiences.belongsTo(Users);

  Seasons.hasMany(Experiences);
  Experiences.belongsTo(Seasons);

  Users.hasMany(Tokens);
  Tokens.belongsTo(Users);

  sequelize
    .sync()
    .then(async () => {
      const settings = [
        Settings.upsert({
          serverID: "471059133597089792",
          leaveCooldown: 120,
          prefix: "?"
        }),
        Settings.upsert({
          serverID: "591547402868228106",
          leaveCooldown: 120,
          prefix: "_"
        })
      ];
      await Promise.all(settings);

      sequelize.close();
    })
    .catch(err => logger.log("error", err));

  callback(null, "Done syncing models.");
}

function initDatabase(callback) {
  async.series([createDatabase, syncModels], (err, result) => {
    if (err) throw logger.log("error", err);

    logger.log("info", result);
    callback(null, `Done initializating database ${database}.`);
  });
}

/*
  EXPORTS
*/
module.exports = initDatabase;
