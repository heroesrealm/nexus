/*
  REQUIREMENTS
*/
const async = require("async");
const Sequelize = require("sequelize");
const {
  database,
  host,
  user,
  password,
  dialect,
  logging
} = require("../settings.json");
const getLogger = require("../utils/functions/getLogger");

/*
  PROPERTIES
*/
const logger = getLogger();
const sequelize = new Sequelize(database, user, password, {
  host,
  dialect,
  logging,
  dialectOptions: {
    useUTC: false // * for reading from database
  },
  timezone: "+02:00" // * for writing to database
});

const Settings = sequelize.import("models/Settings");
const Factions = sequelize.import("models/Factions");
const Users = sequelize.import("models/Users");
const BattleTags = sequelize.import("models/BattleTags");
const Summoners = sequelize.import("models/Summoners");
const Experiences = sequelize.import("models/Experiences.js");
const Seasons = sequelize.import("models/Seasons.js");
const Tokens = sequelize.import("models/Tokens.js");

/*
  FUNCTIONS
*/
function initAssociations(callback) {
  Factions.hasMany(Users);
  Users.belongsTo(Factions);

  Users.hasMany(BattleTags);
  BattleTags.belongsTo(Users);

  Users.hasMany(Summoners);
  Summoners.belongsTo(Users);

  Users.hasMany(Experiences);
  Experiences.belongsTo(Users);

  Seasons.hasMany(Experiences);
  Experiences.belongsTo(Seasons);

  Users.hasMany(Tokens);
  Tokens.belongsTo(Users);

  callback(null, "Done initializating associations.");
}

function initPrototypes(callback) {
  callback(null, "Done initializating prototypes.");
}

function initModels(callback) {
  async.series([initAssociations, initPrototypes], (err, result) => {
    if (err) throw logger.log("error", err);

    logger.log("info", result);
    callback(null, "Done initializating models.");
  });
}

/*
  EXPORTS
*/
module.exports = {
  initModels,
  Settings,
  Factions,
  Users,
  BattleTags,
  Summoners,
  Seasons,
  Experiences,
  Tokens
};
