module.exports = (sequelize, DataTypes) => {
  return sequelize.define("settings", {
    serverID: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    prefix: DataTypes.CHAR,
    joinChannel: DataTypes.STRING,
    joinMessage: DataTypes.TEXT,
    leaveMessage: DataTypes.TEXT,
    leaveCooldown: DataTypes.INTEGER
  });
};
