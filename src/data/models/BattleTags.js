module.exports = (sequelize, DataTypes) => {
  return sequelize.define("battletags", {
    battletag: DataTypes.STRING,
    skillRating: DataTypes.INTEGER
  });
};
