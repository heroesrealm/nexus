module.exports = (sequelize, DataTypes) => {
  return sequelize.define("factions", {
    name: {
      type: DataTypes.STRING,
      unique: true
    },
    description: DataTypes.TEXT,
    color: DataTypes.STRING,
    isJoinable: {
      type: DataTypes.TINYINT,
      defaultValue: false,
      allowNull: false
    }
  });
};
