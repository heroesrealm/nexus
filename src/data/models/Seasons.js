module.exports = (sequelize, DataTypes) => {
  return sequelize.define("seasons", {
    season: {
      type: DataTypes.INTEGER,
      unique: true
    },
    startedAt: DataTypes.STRING,
    endedAt: DataTypes.STRING
  });
};
