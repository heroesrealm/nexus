module.exports = (sequelize, DataTypes) => {
  return sequelize.define("users", {
    discordID: {
      type: DataTypes.STRING,
      unique: true
    },
    discordTag: DataTypes.STRING,
    dateJoinFaction: {
      type: DataTypes.STRING,
      allowNull: true
    }
  });
};
