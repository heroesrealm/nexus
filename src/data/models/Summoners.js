module.exports = (sequelize, DataTypes) => {
  return sequelize.define("summoners", {
    accountName: DataTypes.STRING,
    accountId: DataTypes.STRING,
    soloRank: DataTypes.STRING,
    flexRank: DataTypes.STRING
  });
};
